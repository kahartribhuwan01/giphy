package com.tribhuwan.giphy.listeners

import com.tribhuwan.giphy.model.FavGifModel

interface RecyclerViewItemClickListener {
     fun onItemClick(data: FavGifModel)
}