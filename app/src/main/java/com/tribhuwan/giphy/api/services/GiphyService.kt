package com.tribhuwan.giphy.api.services

import com.tribhuwan.giphy.model.GiphyResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface GiphyService {
    @GET("/v1/gifs/trending")
    suspend fun getTrendingGiphy(
        @Query("api_key") api_key: String,
        @Query("offset") offset: Int,
        @Query("limit") limit: Int
    ):Response<GiphyResponse>

    @GET("v1/gifs/search")
    suspend fun searchGiphy(
        @Query("q") searchText: String,
        @Query("api_key") api_key: String,
        @Query("offset") offset: Int,
        @Query("limit") limit: Int
    ):Response<GiphyResponse>
}