package com.tribhuwan.giphy.api

import com.tribhuwan.giphy.utils.Constants.BASE_URL
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object ServiceGenerator {
    private val builder = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())

    private var retrofit = builder.build()
    private val loggingHeader =
        HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.HEADERS)
    private val loggingBody = HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)
    private val httpClient = OkHttpClient.Builder()

    fun <S> createService(serviceClass: Class<S>): S {
        if (!httpClient.interceptors().contains(loggingHeader)) httpClient.addInterceptor(
            loggingHeader
        )
        if (!httpClient.interceptors().contains(loggingBody)) httpClient.addInterceptor(loggingBody)
        builder.client(httpClient.build())
        return retrofit.create(serviceClass)
    }
}