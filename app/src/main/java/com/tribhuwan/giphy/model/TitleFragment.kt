package com.tribhuwan.giphy.model

import androidx.fragment.app.Fragment

class TitleFragment {
    var title:String? = null
    var fragment:Fragment? = null
    constructor()
    constructor(title: String?, fragment: Fragment?) {
        this.title = title
        this.fragment = fragment
    }
}