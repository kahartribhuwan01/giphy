package com.tribhuwan.giphy.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "favGifModel")
data class FavGifModel(
    @PrimaryKey val id: String,
    val url: String
)