package com.tribhuwan.giphy.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.tribhuwan.giphy.model.TitleFragment

class GiphyViewPagerAdapter(
    private val fragmentManager: FragmentManager,
    private val fragmentList: List<TitleFragment>
) : FragmentPagerAdapter(
    fragmentManager,
    BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT
) {
    override fun getCount(): Int = fragmentList.size


    override fun getItem(position: Int): Fragment = fragmentList[position].fragment!!
    override fun getPageTitle(position: Int): CharSequence? {
        return fragmentList[position].title!!
    }
}