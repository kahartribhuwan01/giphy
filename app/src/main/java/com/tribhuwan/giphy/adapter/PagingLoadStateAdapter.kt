package com.tribhuwan.giphy.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.paging.LoadState
import androidx.paging.LoadStateAdapter
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.RecyclerView
import com.tribhuwan.giphy.databinding.ItemNetworkStateBinding

class PagingLoadStateAdapter<T : Any, VH : RecyclerView.ViewHolder>(private val adapter: PagingDataAdapter<T, VH>) :
    LoadStateAdapter<PagingLoadStateAdapter.NetworkStateItemViewHolder>() {


    class NetworkStateItemViewHolder(
        private val binding: ItemNetworkStateBinding,
        private val recycleCallBack: () -> Unit
    ) :
        RecyclerView.ViewHolder(binding.root) {

        init {
            binding.retry.setOnClickListener {
                recycleCallBack()
            }
        }

        fun bind(loadState: LoadState) {
            with(binding) {
                progressBar.isVisible = loadState is LoadState.Loading
                retry.isVisible = loadState is LoadState.Error
                noInternetMessage.isVisible =
                    !(loadState as? LoadState.Error)?.error?.message.isNullOrBlank()
                noInternetMessage.text = (loadState as? LoadState.Error)?.error?.message
            }
        }
    }

    override fun onBindViewHolder(holder: NetworkStateItemViewHolder, loadState: LoadState) {
        return holder.bind(loadState)
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        loadState: LoadState
    ): NetworkStateItemViewHolder = NetworkStateItemViewHolder(
        ItemNetworkStateBinding.inflate(LayoutInflater.from(parent.context), parent, false)
    ) {
        adapter.retry()
    }

}