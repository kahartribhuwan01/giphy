package com.tribhuwan.giphy.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.tribhuwan.giphy.R
import com.tribhuwan.giphy.databinding.RowGiphyBinding
import com.tribhuwan.giphy.listeners.RecyclerViewItemClickListener
import com.tribhuwan.giphy.model.Data
import com.tribhuwan.giphy.model.FavGifModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext


class GiphyAdapter(private val listener: RecyclerViewItemClickListener,
) :
    PagingDataAdapter<Data, GiphyAdapter.GiphyViewHolder>(DIFF_UTIL) {

    companion object {
        val DIFF_UTIL = object : DiffUtil.ItemCallback<Data>() {
            override fun areItemsTheSame(oldItem: Data, newItem: Data): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: Data, newItem: Data): Boolean {
                return oldItem == newItem
            }

        }
    }

    inner class GiphyViewHolder(val rowGiphyBinding: RowGiphyBinding) :
        RecyclerView.ViewHolder(rowGiphyBinding.root)

    override fun onBindViewHolder(holder: GiphyViewHolder, position: Int) {
        with(getItem(position)) {
            val circularProgressDrawable = CircularProgressDrawable(holder.itemView.context)
            circularProgressDrawable.strokeWidth = 10f
            circularProgressDrawable.centerRadius = 50f
            circularProgressDrawable.start()

            val requestOptions = RequestOptions()
            requestOptions.placeholder(circularProgressDrawable)
            requestOptions.error(R.drawable.ic_error)
            requestOptions.skipMemoryCache(true)
            Glide.with(holder.itemView.context)
                .asGif()
                .apply(requestOptions)
                .transition(DrawableTransitionOptions.withCrossFade())
                .load(this?.images?.original?.url)
                .placeholder(requestOptions.placeholderDrawable)
                .into(holder.rowGiphyBinding.gif)

            holder.rowGiphyBinding.addToFav.setOnClickListener {
                val favGif = FavGifModel(this?.id!!, this.images?.original?.url!!)
                    listener.onItemClick(favGif)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GiphyViewHolder {
        val binding = RowGiphyBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return GiphyViewHolder(binding)
    }
}