package com.tribhuwan.giphy.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.tribhuwan.giphy.R
import com.tribhuwan.giphy.databinding.RowFavGiphyBinding
import com.tribhuwan.giphy.listeners.RecyclerViewItemClickListener
import com.tribhuwan.giphy.model.FavGifModel
import java.lang.Exception


class FavGifAdapter(
    private val listener: RecyclerViewItemClickListener) : PagingDataAdapter<FavGifModel, FavGifAdapter.GiphyViewHolder>(DIFF_UTIL) {

    companion object {
        val DIFF_UTIL = object : DiffUtil.ItemCallback<FavGifModel>() {
            override fun areItemsTheSame(oldItem: FavGifModel, newItem: FavGifModel): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: FavGifModel, newItem: FavGifModel): Boolean {
                return oldItem == newItem
            }

        }
    }

    inner class GiphyViewHolder(val rowGiphyBinding: RowFavGiphyBinding) :
        RecyclerView.ViewHolder(rowGiphyBinding.root)

    override fun onBindViewHolder(holder: GiphyViewHolder, position: Int) {
        with(getItem(position)) {
            try {
                val circularProgressDrawable = CircularProgressDrawable(holder.itemView.context)
                circularProgressDrawable.strokeWidth = 10f
                circularProgressDrawable.centerRadius = 50f
                circularProgressDrawable.start()

                val requestOptions = RequestOptions()
                requestOptions.placeholder(circularProgressDrawable)
                requestOptions.error(R.drawable.ic_error)
                requestOptions.skipMemoryCache(true)

                Glide.with(holder.itemView.context)
                    .asGif()
                    .apply(requestOptions)
                    .transition(DrawableTransitionOptions.withCrossFade())
                    .load(this?.url)
                    .placeholder(requestOptions.placeholderDrawable)
                    .into(holder.rowGiphyBinding.gif)

                holder.rowGiphyBinding.deleteFromFav.setOnClickListener {
                    listener.onItemClick(this@with!!)
                }
            } catch (e: Exception) {
                Toast.makeText(
                    holder.itemView.context,
                    "Something went wrong!",
                    Toast.LENGTH_SHORT
                ).show()
            }

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GiphyViewHolder {
        val binding = RowFavGiphyBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return GiphyViewHolder(binding)
    }
}