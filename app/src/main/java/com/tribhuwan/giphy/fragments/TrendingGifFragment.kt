package com.tribhuwan.giphy.fragments

import android.graphics.Color
import android.graphics.Color.*
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.widget.SearchView
import androidx.core.view.isVisible
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.room.withTransaction
import com.tribhuwan.giphy.MainActivity
import com.tribhuwan.giphy.R
import com.tribhuwan.giphy.adapter.GiphyAdapter
import com.tribhuwan.giphy.adapter.PagingLoadStateAdapter
import com.tribhuwan.giphy.databinding.FragmentTrendingBinding
import com.tribhuwan.giphy.db.AppDB
import com.tribhuwan.giphy.listeners.RecyclerViewItemClickListener
import com.tribhuwan.giphy.model.FavGifModel
import com.tribhuwan.giphy.vm.GiphyViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

@AndroidEntryPoint
class TrendingGifFragment() : Fragment(), RecyclerViewItemClickListener {

    private val giphyViewModel: GiphyViewModel by viewModels()
    private lateinit var giphyAdapter: GiphyAdapter
    private lateinit var fragmentTrendingBinding: FragmentTrendingBinding
    private lateinit var appDB : AppDB


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        giphyAdapter = GiphyAdapter(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        fragmentTrendingBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_trending, container, false)
        return fragmentTrendingBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        appDB = AppDB.getDatabase(context!!)
        with(fragmentTrendingBinding) {
            val searchTextView =
                searchGifs.findViewById<TextView>(androidx.appcompat.R.id.search_src_text)
            searchTextView.setTextColor(WHITE)
            searchTextView.setHintTextColor(GRAY)
            searchGifs.onActionViewExpanded()
            searchGifs.clearFocus()

            rvGiphy.apply {
                layoutManager =
                    LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
                adapter = giphyAdapter
            }
            with(giphyAdapter) {
                rvGiphy.adapter = withLoadStateHeaderAndFooter(
                    header = PagingLoadStateAdapter(this),
                    footer = PagingLoadStateAdapter(this)
                )
            }
            swipeRefresh.setOnRefreshListener { giphyAdapter.refresh() }

            searchGifs.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String): Boolean {
                    giphyViewModel.setSearchQuery(query)
                    return false
                }

                override fun onQueryTextChange(newText: String): Boolean {
                    if (newText.isEmpty()) {
                        giphyViewModel.setSearchQuery(newText)
                    }
                    return false
                }
            })
        }

        with(giphyViewModel) {
            launchOnLifecycleScope {
                delay(1000)
                giphyList.collectLatest {
                    fragmentTrendingBinding.loading.isVisible = false
                    fragmentTrendingBinding.swipeRefresh.isRefreshing = false
                    fragmentTrendingBinding.rvGiphy.isVisible = true
                    giphyAdapter.submitData(it)
                }
            }
            launchOnLifecycleScope {
                searchedGif.collectLatest {
                    fragmentTrendingBinding.rvGiphy.scrollToPosition(0)
                    fragmentTrendingBinding.swipeRefresh.isRefreshing = false
                    giphyAdapter.submitData(it)
                }
            }
        }
        launchOnLifecycleScope {
            giphyAdapter.addLoadStateListener { loadState ->
                val isListEmpty =
                    loadState.refresh is LoadState.Loading && giphyAdapter.itemCount == 0
                with(fragmentTrendingBinding) {
                    tvEmpty.isVisible = isListEmpty
                    rvGiphy.isVisible =
                        loadState.source.refresh is LoadState.NotLoading || loadState.mediator?.refresh is LoadState.NotLoading
                    loading.isVisible = loadState.mediator?.refresh is LoadState.Loading
                    val errorState = loadState.source.append as? LoadState.Error
                        ?: loadState.source.prepend as? LoadState.Error
                        ?: loadState.append as? LoadState.Error
                        ?: loadState.prepend as? LoadState.Error

                    errorState.let {
                        tvEmpty.text = it?.error?.message
                    }

                    if (loadState.source.refresh is LoadState.NotLoading && loadState.append.endOfPaginationReached && giphyAdapter.itemCount < 1) {
                        rvGiphy.isVisible = false
                        tvEmpty.isVisible = true
                    } else {
                        rvGiphy.isVisible = true
                        tvEmpty.isVisible = false
                    }
                }

            }
        }

    }

    private fun launchOnLifecycleScope(execute: suspend () -> Unit) {
        lifecycleScope.launch {
            execute()
        }
    }

    override fun onItemClick(data: FavGifModel) {
        lifecycleScope.launch {
            appDB.withTransaction {
                appDB.favGifDao().insert(data)
            }
        }
        (activity as MainActivity).favItemInserted.value = true
        Toast.makeText(context, "Gif added to favourites!", Toast.LENGTH_SHORT).show()
    }

}