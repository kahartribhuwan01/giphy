package com.tribhuwan.giphy.fragments

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import androidx.recyclerview.widget.GridLayoutManager
import com.tribhuwan.giphy.MainActivity
import com.tribhuwan.giphy.R
import com.tribhuwan.giphy.adapter.FavGifAdapter
import com.tribhuwan.giphy.adapter.PagingLoadStateAdapter
import com.tribhuwan.giphy.databinding.FragmentFavouriteGifBinding
import com.tribhuwan.giphy.db.AppDB
import com.tribhuwan.giphy.listeners.RecyclerViewItemClickListener
import com.tribhuwan.giphy.model.FavGifModel
import com.tribhuwan.giphy.vm.GiphyViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

@AndroidEntryPoint
class FavouriteGifFragment() : Fragment(), RecyclerViewItemClickListener {
    private val giphyViewModel: GiphyViewModel by viewModels()
    private lateinit var favGifAdapter: FavGifAdapter
    private lateinit var fragmentFavouriteGifBinding: FragmentFavouriteGifBinding
    private lateinit var appDB: AppDB

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        favGifAdapter = FavGifAdapter(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        fragmentFavouriteGifBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_favourite_gif, container, false)

        return fragmentFavouriteGifBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        appDB = AppDB.getDatabase(context!!)
        with(fragmentFavouriteGifBinding) {
            swipeRefresh.setOnRefreshListener {
                favGifAdapter.refresh()
            }
            rvFavGif.apply {
                layoutManager = GridLayoutManager(context, 2)
                adapter = favGifAdapter
            }
            with(favGifAdapter) {
                rvFavGif.adapter = withLoadStateHeaderAndFooter(
                    header = PagingLoadStateAdapter(this),
                    footer = PagingLoadStateAdapter(this)
                )
            }
        }
        launchOnLifecycleScope {
            favGifAdapter.loadStateFlow.collectLatest { loadState ->
                val isListEmpty =
                    loadState.refresh is LoadState.Loading && favGifAdapter.itemCount == 0
                with(fragmentFavouriteGifBinding) {
                    tvEmpty.isVisible = isListEmpty
                    rvFavGif.isVisible =
                        loadState.source.refresh is LoadState.NotLoading || loadState.mediator?.refresh is LoadState.NotLoading
                    loading.isVisible = loadState.mediator?.refresh is LoadState.Loading
                    val errorState = loadState.source.append as? LoadState.Error
                        ?: loadState.source.prepend as? LoadState.Error
                        ?: loadState.append as? LoadState.Error
                        ?: loadState.prepend as? LoadState.Error

                    errorState.let {
                        tvEmpty.text = it?.error?.message
                    }
                }

            }
        }

        with(giphyViewModel) {
            launchOnLifecycleScope {
                favGiphyList.collectLatest {
                    fragmentFavouriteGifBinding.swipeRefresh.isRefreshing = false
                    favGifAdapter.submitData(it)
                }
            }
            (activity as MainActivity).favItemInserted.observe(viewLifecycleOwner) {
               Handler(Looper.getMainLooper()).postDelayed({
                   favGifAdapter.refresh()
                   favGifAdapter.notifyDataSetChanged()
               },50)
            }
        }
    }

    private fun launchOnLifecycleScope(execute: suspend () -> Unit) {
        viewLifecycleOwner.lifecycleScope.launchWhenCreated {
            execute()
        }
    }

    override fun onItemClick(data: FavGifModel) {
        lifecycleScope.launch {
            appDB.favGifDao().deleteById(data.id)
        }
        favGifAdapter.refresh()
        Toast.makeText(context, "Gif removed from favourites!", Toast.LENGTH_SHORT).show()
    }
}