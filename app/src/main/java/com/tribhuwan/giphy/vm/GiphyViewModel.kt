package com.tribhuwan.giphy.vm

import androidx.lifecycle.*
import androidx.paging.*
import com.tribhuwan.giphy.model.Data
import com.tribhuwan.giphy.model.FavGifModel
import com.tribhuwan.giphy.repo.GiphyRepo
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.ConflatedBroadcastChannel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import java.lang.Exception
import javax.inject.Inject

@HiltViewModel
class GiphyViewModel @Inject constructor(
    private val giphyRepo: GiphyRepo,
    private val savedStateHandle: SavedStateHandle
) : ViewModel() {

    private val searchGif = ConflatedBroadcastChannel<String>()

    private var _giphyList: Flow<PagingData<Data>>
    val giphyList: Flow<PagingData<Data>>
        get() = _giphyList


    private lateinit var _favGiphyList: Flow<PagingData<FavGifModel>>
    val favGiphyList: Flow<PagingData<FavGifModel>>
        get() = _favGiphyList

    init {
        fetchFavGiphy()
        val initialQuery: String = savedStateHandle.get(LAST_SEARCH_QUERY) ?: DEFAULT_QUERY
        val actionStateFlow = MutableSharedFlow<UiAction>()
        val searches = actionStateFlow
            .filterIsInstance<UiAction.Search>()
            .distinctUntilChanged()
            .onStart { emit(UiAction.Search(query = initialQuery)) }
        _giphyList = searches
            .flatMapLatest { fetchGiphy(searchText = it.query) }
            .cachedIn(viewModelScope)

    }

    fun setSearchQuery(search: String) {
        searchGif.offer(search)
    }

    val searchedGif = searchGif.asFlow()
        .flatMapLatest {
            fetchGiphy(searchText = it)
        }.cachedIn(viewModelScope)

    fun fetchGiphy(searchText: String): Flow<PagingData<Data>> {
        return giphyRepo.fetchGiphy(searchText).cachedIn(viewModelScope)
    }

    fun fetchFavGiphy() = launchPagingAsync({
        giphyRepo.favGiphy().cachedIn(viewModelScope)
    }, {
        _favGiphyList = it
    })

    private inline fun <T> launchPagingAsync(
        crossinline execute: suspend () -> Flow<T>,
        crossinline onSuccess: (Flow<T>) -> Unit
    ) {
        viewModelScope.launch {
            try {
                val result = execute()
                onSuccess(result)
            } catch (ex: Exception) {

            }
        }
    }
}

sealed class UiAction {
    data class Search(val query: String) : UiAction()
    data class Scroll(val currentQuery: String) : UiAction()
}

private const val LAST_SEARCH_QUERY: String = "last_search_query"
private const val DEFAULT_QUERY = ""