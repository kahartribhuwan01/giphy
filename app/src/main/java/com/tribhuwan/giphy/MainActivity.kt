package com.tribhuwan.giphy

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.MutableLiveData
import com.tribhuwan.giphy.adapter.GiphyViewPagerAdapter
import com.tribhuwan.giphy.databinding.ActivityMainBinding
import com.tribhuwan.giphy.model.TitleFragment
import dagger.hilt.android.AndroidEntryPoint
import com.tribhuwan.giphy.fragments.FavouriteGifFragment
import com.tribhuwan.giphy.fragments.TrendingGifFragment
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlin.coroutines.CoroutineContext

@AndroidEntryPoint
class MainActivity : AppCompatActivity()  {
    private lateinit var activityMainBinding: ActivityMainBinding
    val favItemInserted = MutableLiveData<Boolean>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        with(activityMainBinding) {
            val trending = TitleFragment("Trending", TrendingGifFragment())
            val favourites = TitleFragment("Favourites", FavouriteGifFragment())

            val fragmentList = ArrayList<TitleFragment>()
            fragmentList.add(trending)
            fragmentList.add(favourites)

            val giphyViewPagerAdapter = GiphyViewPagerAdapter(supportFragmentManager, fragmentList)
            giphyViewPager.adapter = giphyViewPagerAdapter
            tabs.setupWithViewPager(giphyViewPager)

        }
    }
}