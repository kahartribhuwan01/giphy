package com.tribhuwan.giphy.di

import com.tribhuwan.giphy.api.ServiceGenerator
import com.tribhuwan.giphy.api.services.GiphyService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@InstallIn(SingletonComponent::class)
@Module
object DomainModules {
    @Provides
    fun getGiphyService(): GiphyService{
        return  ServiceGenerator.createService(GiphyService::class.java)
    }
}