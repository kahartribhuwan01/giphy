package com.tribhuwan.giphy.di

import com.tribhuwan.giphy.repo.GiphyRepo
import com.tribhuwan.giphy.repo.GiphyRepoImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class RepoModule {
    @Binds
    abstract fun bindGiphyRepo(giphyRepoImpl: GiphyRepoImpl): GiphyRepo
}