package com.tribhuwan.giphy.di

import android.content.Context
import com.tribhuwan.giphy.db.AppDB
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class DBModule {

    @Singleton
    @Provides
    fun provideDB(@ApplicationContext applicationContext: Context) =
        AppDB.getDatabase(applicationContext)

}