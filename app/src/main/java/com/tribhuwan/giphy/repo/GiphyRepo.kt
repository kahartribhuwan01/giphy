package com.tribhuwan.giphy.repo

import androidx.paging.PagingData
import com.tribhuwan.giphy.model.Data
import com.tribhuwan.giphy.model.FavGifModel
import kotlinx.coroutines.flow.Flow

interface GiphyRepo {
    fun fetchGiphy(searchText: String): Flow<PagingData<Data>>
    suspend fun favGiphy(): Flow<PagingData<FavGifModel>>
}