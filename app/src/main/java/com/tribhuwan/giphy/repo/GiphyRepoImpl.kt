package com.tribhuwan.giphy.repo

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import com.tribhuwan.giphy.api.services.GiphyService
import com.tribhuwan.giphy.datasource.FavouriteGifDataSource
import com.tribhuwan.giphy.datasource.GiphyDataSource
import com.tribhuwan.giphy.db.AppDB
import com.tribhuwan.giphy.model.Data
import com.tribhuwan.giphy.model.FavGifModel
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GiphyRepoImpl @Inject constructor(
    private val giphyService: GiphyService,
    private val appDB: AppDB
) : GiphyRepo {
    override  fun fetchGiphy(searchText: String): Flow<PagingData<Data>> =
        Pager(
            PagingConfig(
                pageSize = 10,
                initialLoadSize = 10,
                enablePlaceholders = false,
                prefetchDistance = 2
            )
        ) {
            GiphyDataSource(searchText, giphyService)
        }.flow

    override suspend fun favGiphy(): Flow<PagingData<FavGifModel>> =
        Pager(
            PagingConfig(
                pageSize = 10,
                initialLoadSize = 10,
                enablePlaceholders = false,
                prefetchDistance = 2
            )
        ) {
            FavouriteGifDataSource(appDB)
        }.flow
}