package com.tribhuwan.giphy.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.tribhuwan.giphy.model.Data
import com.tribhuwan.giphy.model.FavGifModel

@Dao
interface FavouriteGifDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(giphyData: FavGifModel)

    @Query("SELECT * FROM favGifModel LIMIT :limit OFFSET :offset")
    suspend fun fetchAllFavouriteGif(limit: Int, offset: Int): List<FavGifModel>

    @Query("DELETE FROM favGifModel WHERE id = :id")
    suspend fun deleteById(id: String)
}