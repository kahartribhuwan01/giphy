package com.tribhuwan.giphy.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.tribhuwan.giphy.db.dao.FavouriteGifDao
import com.tribhuwan.giphy.model.FavGifModel

@Database(entities = [FavGifModel::class], version = 1, exportSchema = false)
abstract class AppDB : RoomDatabase() {
    abstract fun favGifDao(): FavouriteGifDao

    companion object {
        @Volatile
        private var instance: AppDB? = null

        fun getDatabase(context: Context): AppDB = instance ?: synchronized(this) {
            instance ?: buildDatabase(context)
        }

        private fun buildDatabase(context: Context) =
            Room.databaseBuilder(context, AppDB::class.java, "tribhuwanGiphy")
                .fallbackToDestructiveMigration()
                .build()
    }
}