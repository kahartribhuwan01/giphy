package com.tribhuwan.giphy.datasource

import androidx.paging.LoadState
import androidx.paging.PagingSource
import androidx.paging.PagingState
import androidx.room.withTransaction
import com.tribhuwan.giphy.db.AppDB
import com.tribhuwan.giphy.db.dao.FavouriteGifDao
import com.tribhuwan.giphy.model.Data
import com.tribhuwan.giphy.model.FavGifModel
import kotlinx.coroutines.delay
import java.lang.Exception

class FavouriteGifDataSource(private val appDB: AppDB) :
    PagingSource<Int, FavGifModel>() {
    private val favouriteGifDao = appDB.favGifDao()

    override fun getRefreshKey(state: PagingState<Int, FavGifModel>): Int? {
        return state.anchorPosition?.let { anchorPosition ->
            val anchorPage = state.closestPageToPosition(anchorPosition)
            anchorPage?.prevKey?.plus(1) ?: anchorPage?.nextKey?.minus(1)
        }
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, FavGifModel> {
        val pageNumber = params.key ?: INITAIL_PAGE
        return try {
            val favGifs = appDB.withTransaction {
                favouriteGifDao.fetchAllFavouriteGif(params.loadSize, pageNumber * params.loadSize)
            }
            if (pageNumber != 0) delay(1000)
            LoadResult.Page(
                data = favGifs,
                prevKey = if (pageNumber == 0) null else pageNumber - 1,
                nextKey = if (favGifs.isEmpty()) null else pageNumber + 1
            )
        }catch (e:Exception){
            LoadResult.Error(e)
        }
    }

    companion object {
        const val INITAIL_PAGE = 0
    }
}