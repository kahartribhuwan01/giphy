package com.tribhuwan.giphy.datasource

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.tribhuwan.giphy.api.services.GiphyService
import com.tribhuwan.giphy.model.Data
import com.tribhuwan.giphy.utils.Constants
import kotlinx.coroutines.delay
import java.lang.Exception

class GiphyDataSource(val searchText: String, private val giphyService: GiphyService) :
    PagingSource<Int, Data>() {

    override fun getRefreshKey(state: PagingState<Int, Data>): Int? {
        return state.anchorPosition?.let {
            val anchorPage = state.closestPageToPosition(it)
            anchorPage?.prevKey?.plus(1) ?: anchorPage?.nextKey?.minus(1)
        }
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Data> {
        val pageNumber = params.key ?: 0
        return try {
            val data = if (searchText.isEmpty()) giphyService.getTrendingGiphy(
                Constants.API_KEY,
                pageNumber * params.loadSize, params.loadSize
            ) else giphyService.searchGiphy(
                searchText, Constants.API_KEY,
                pageNumber * params.loadSize, params.loadSize
            )
            if (pageNumber != 0) delay(2000)
            LoadResult.Page(
                data = data.body()?.data!!,
                prevKey = if (pageNumber == 0) null else pageNumber - 1,
                nextKey = if (data.body()?.data!!.isEmpty()) null else pageNumber + 1
            )
        } catch (e: Exception) {
            LoadResult.Error(e)
        }
    }
}